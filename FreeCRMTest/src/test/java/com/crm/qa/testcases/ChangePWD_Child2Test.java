package com.crm.qa.testcases;

import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ChangePWD_Child2Test extends TestBase{
	
	

  @BeforeTest
  public void beforeTest() {
  }

  
@Test (priority=9)
  public void AllBlank1() {
	 
	  driver.findElement(By.xpath("//a[text()='Change Password']")).click();
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath("//div[@id='passwordBlank']")).getText();
	  String expected_result = "Current password cannot be empty";
	  Assert.assertEquals(actual_result, expected_result);
	  
	  System.out.println("The Error Message is : " + actual_result);
	   
  }
  
  
@Test (priority=10)
  
  public void NewPWDBlank() {
	  
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//input[@id='passwordc']")).sendKeys("Divyang@2026");
	  driver.findElement(By.xpath("//input[@id='password2']")).sendKeys("Divyang@2026");
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath("//div[@id='password1Blank']")).getText();
	  String expected_result = "New password cannot be empty";
	  Assert.assertEquals(actual_result, expected_result);
	  
	  System.out.println("The Error Message is : " + actual_result);
  }
  

@Test (priority=11)
  
  public void ConfirmPWDBlank() {
	  
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//input[@id='passwordc']")).sendKeys("Divyang@2026"); //input[@id='passwordc']
	  driver.findElement(By.xpath("//input[@id='password1']")).sendKeys("Divyang@2026");  //input[@id='password1']
	  driver.findElement(By.xpath("//input[@value='Change Password']")).click();   //input[@value='Change Password']
	  
	  String actual_result = driver.findElement(By.xpath("//div[@id='password2Blank']")).getText();  ////div[@id='password2Blank']
	  String expected_result = "Confirm password cannot be empty";
	  Assert.assertEquals(actual_result, expected_result);
	  
	  System.out.println("The Error Message is : " + actual_result);
  }
  
   
@Test (priority=12)
  
  public void CurrentPWDBlank (  ) {
	
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//input[@id='password1']")).sendKeys("Divyang@2026");
	  driver.findElement(By.xpath("//input[@id='password2']")).sendKeys("Divyang@2026");
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath("//div[@id='passwordBlank']")).getText();
	  String expected_result = "Current password cannot be empty";
	  Assert.assertEquals(actual_result, expected_result);
	  
	  System.out.println("The Error Message is : " + actual_result);
	  
  }
  
 
@Test (priority=13)
  
  public void WrongCurrentPWD () throws InterruptedException  {
	  
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//input[@id='passwordc']")).sendKeys("Abcd@1234");
	  driver.findElement(By.xpath("//input[@id='password1']")).sendKeys("Divyang@2026");
	  driver.findElement(By.xpath("//input[@id='password2']")).sendKeys("Divyang@2026");
	  driver.findElement(By.xpath("//input[@value='Change Password']")).click();
	  Thread.sleep(5000);
	  
	  String actual_result = driver.findElement(By.xpath("//*[@id=\"notification_7d709e73b749f2af1ca1da55361c2453\"]/div/div[2]")).getText();
	  String expected_result = "Wrong ! Password please try again.";
	  Assert.assertEquals(actual_result, expected_result);
	  
	  System.out.println("The Error Message is : " + actual_result);
	  
  }
  
  
  
  @AfterTest
  public void afterTest() {
  }

}
