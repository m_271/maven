package com.crm.qa.testcases;

import org.testng.annotations.Test;

import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;
import com.crm.qa.base.TestBase;


import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class ShopcluesParentTest1 extends TestBase {
	LoginPage loginPage;
	HomePage homePage;
	
	public ShopcluesParentTest1() {
		super();
	}
	
	
	
	@BeforeClass
	  public void beforeClass() throws IOException {
		 initialization();
		 loginPage = new LoginPage();
	  }	
  @Test (priority=0)
  public void ShopcluesLogin() throws InterruptedException {
	  Thread.sleep(5000);	
	   driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
	  Thread.sleep(5000);
	  driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
	  Thread.sleep(5000);

	  homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		
		//WebDriverWait wait1 = new WebDriverWait(driver,20);
		//wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='login_button']"))).click();
		
		driver.findElement(By.xpath("(//div/a[text()='Skip'])[2]")).click();
		Thread.sleep(5000);
		
  }
 

  @AfterClass
  public void afterClass() {
  }

}
