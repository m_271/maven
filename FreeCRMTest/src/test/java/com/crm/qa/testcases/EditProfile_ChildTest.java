package com.crm.qa.testcases;

import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;

import org.testng.annotations.BeforeTest;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class EditProfile_ChildTest extends TestBase{
  
  @BeforeTest
  public void beforeTest() {
  }
  
  @Test (priority=1)
  public void FirstNameBlank() {
	  
	  Actions action = new Actions (driver);
	  WebElement mo = driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
	  action.moveToElement(mo).perform();
	  //driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a")).click();
	  WebDriverWait wait = new WebDriverWait(driver,20);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[contains(@href,'https://myaccount.shopclues.com/index.php?dispatch=profiles.update')])[1]"))).click();
	  //driver.findElement(By.xpath("//a/i[@class='usr_profile']")).click();
	  driver.findElement(By.xpath("//input[@id='firstname']")).clear();
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath("(//div[@class='error-text'])[2]")).getText();
	  String expected_result = "First name cannot be blank";
	  Assert.assertEquals(actual_result, expected_result);
  }

  @Test (priority=2)
  
  public void LastNameBlank() {
	  
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//input[@id='lastname']")).clear();
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath("(//div[@class='error-text'])[4]")).getText();
	  String expected_result = "Last name cannot be blank";
	  Assert.assertEquals(actual_result, expected_result);
	  
  }
  
  @Test (priority=3)
  
  public void PhoneBlank() throws InterruptedException {
	  
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//input[@id='phone']")).clear();
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath(" //div/span[text()='Mobile number cannot be blank']")).getText();
	  String expected_result = "Mobile number cannot be blank";
	  Assert.assertEquals(actual_result, expected_result);
	  Thread.sleep(5000);
	  
  }
  
  @Test (priority=4)
  
  public void AllBlank() {
	  
	  driver.navigate().refresh();
	  driver.findElement(By.xpath("//input[@id='firstname']")).clear();
	  driver.findElement(By.xpath("//input[@id='lastname']")).clear();
	  driver.findElement(By.xpath("//input[@id='phone']")).clear();
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result=driver.findElement(By.xpath("//div/span[text()='First name cannot be blank']")).getText();
	  String expected_result="First name cannot be blank";
	  Assert.assertEquals(actual_result, expected_result);
	  
  }
  
  @Test (priority=5)
  
  public void MinimumCharcater() {
	  
	 driver.navigate().refresh();
	  
	  driver.findElement(By.xpath("//input[@id='firstname']")).clear();
	  driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("D");
	  
	  driver.findElement(By.xpath("//input[@id='lastname']")).clear();
	  driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("D");
	  
	  driver.findElement(By.xpath("//input[@id='phone']")).clear();
	  driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("1");
	  
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath("//div/span[text()='Enter correct mobile number']")).getText();
	  //String actual_result = driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
	  /*WebDriverWait wait = new WebDriverWait (driver,20);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div/span[text()='Enter correct mobile number']"))).getText();*/
	  String expected_result = "Enter correct mobile number";
	  Assert.assertEquals(actual_result, expected_result);
	  
  }
  
  @Test (priority=6)
  
  public void JunkCharcter() {
	  
	  driver.navigate().refresh();
	  
	  driver.findElement(By.xpath("//input[@id='firstname']")).clear();
	  driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("!@#$%^&*()");
	  
	  driver.findElement(By.xpath("//input[@id='lastname']")).clear();
	  driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("!@#$%^&*()/*-+");
	  
	  driver.findElement(By.xpath("//input[@id='phone']")).clear();
	  driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("!@#$%^&*()");
	  
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  //String actual_result = driver.findElement(By.xpath("//div/span[text()='Enter correct first name']")).getText();
	  String actual_result = driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
	  String expected_result = "Enter correct first name";
	  Assert.assertEquals(actual_result, expected_result);
	  
	  // last name with junk character
	  driver.navigate().refresh();
	  
	  WebDriverWait wait = new WebDriverWait (driver,20);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='lastname']"))).clear();
	  driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("!@#$%^&*()/*-+");
	  
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result1 = driver.findElement(By.xpath("//div/span[text()='Enter correct last name']")).getText();
	  String expected_result1 = "Enter correct last name";
	  Assert.assertEquals(actual_result1, expected_result1);
	  
	  //phone number with junk character
	  
	  driver.navigate().refresh();
	  
	  WebDriverWait wait2 = new WebDriverWait (driver,20);
	  wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='phone']"))).clear();
	  driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("!@#$%^&*()");
	  
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result2 = driver.findElement(By.xpath("//div/span[text()='Enter correct mobile number']")).getText();
	  String expected_result2 = "Enter correct mobile number";
	  Assert.assertEquals(actual_result2, expected_result2);
	  
  }
  
  @Test (priority=7)
  
  public void Trim() {
	  
	  driver.navigate().refresh();
	  
	  WebDriverWait wait = new WebDriverWait (driver,20);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='firstname']"))).clear();
	  driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("         D");
	  
	  driver.findElement(By.xpath("//input[@id='lastname']")).clear();
	  driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("        D");
	  
	  driver.findElement(By.xpath("//input[@id='phone']")).clear();
	  driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("    12345");
	  
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  String actual_result = driver.findElement(By.xpath("//div/span[text()='Enter correct mobile number']")).getText();
	  String expected_result = "Enter correct mobile number";
	  Assert.assertEquals(actual_result, expected_result);
	  
  }
  
  @Test (priority=8)
  
  public void ValidInfo() throws InterruptedException  {
	  
	  driver.navigate().refresh();
	  
	  WebDriverWait wait = new WebDriverWait (driver,20);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='firstname']"))).clear();
	  driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("D");
	  
	  driver.findElement(By.xpath("//input[@id='lastname']")).clear();
	  driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("D");
	  
	  driver.findElement(By.xpath("//input[@id='phone']")).clear();
	  driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("1234567890");
	  
	  driver.findElement(By.xpath("//input[@id='save_profile_but']")).click();
	  
	  WebDriverWait wait2 = new WebDriverWait (driver,20);
	  wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[@for='gnd-radio-1-1']"))).click();
	  //driver.findElement(By.xpath("//label[@for='gnd-radio-1-1']")).click();
	  
	  WebDriverWait wait1 = new WebDriverWait (driver,20);
	  //wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='notification-n']"))).getText();
	  wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='notification-header-n']//following::div[contains(text(),'Profile has been updated successfully.')]"))).getText();
	  String expected_result = "Profile has been updated successfully.";
	  
	  Assert.assertEquals(wait1, expected_result);
	  
	  System.out.println(wait1);
	  
	  String actual_result1 = driver.findElement(By.xpath("(//a[contains(@href,'javascript:void(0);')])[2]")).getText();
	  String expected_result1 = "Hi\n" + 
	  		"								    					D\n" + 
	  		"				    								";
	  Assert.assertEquals(actual_result1, expected_result1);
	  System.out.println(actual_result1);
	  
	  Thread.sleep(5000);
	  
  }
  
  
  @AfterTest
  public void afterTest() {
  }

}
